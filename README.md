# recordquran.org

بسم الله

This is the source code for this website: https://recordquran.org

The recordquran is a project that collects audio Quran recitations from people. The data will be in the public domain for anyone to use for any purpose (Research, Education... etc).

The project development is done on trello: https://trello.com/b/WHXusnz7/recordquranorg

If you're interested in contribution contact me: adhamzahranfms@gmail.com

Bye bye :smile: Salam.