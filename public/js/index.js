import {
    change_language,
    populate_suras,
    populate_numbers_dropdown,
    finished_recording_message,
    upload_in_progress_message
} from "./languages.js";

import { get_aya_text as get_aya_text_ } from "./all_ayat.js";

import { upload_recording } from "./service.js";

import { start_recording, stop_recording } from "./record.js";

const local_storage = window.localStorage;

const record_btn = document.getElementById("record_btn");
const stop_btn = document.getElementById("stop_btn");
const next_btn = document.getElementById("next_btn");
const timer = document.getElementById("timer");
const minutes = document.getElementById("minutes");
const seconds = document.getElementById("seconds");
const sura = document.getElementById("sura");
const aya = document.getElementById("aya");
const aya_text = document.getElementById("aya_text");
const just_a_sec = document.getElementById("just_a_sec");

const age = Number(local_storage.getItem('age'));
const gender = local_storage.getItem('gender');
const tongue = local_storage.getItem('tongue');
const speaker_id = local_storage.getItem('id');

let selected_sura = Number(local_storage.getItem('sura'));
let selected_aya = Number(local_storage.getItem('aya'));

window.AudioContext = window.AudioContext || window.webkitAudioContext;
window.URL = window.URL || window.webkitURL;

if(selected_sura === 0 ||
   selected_aya  === 0)
{
    selected_sura = 1;
    selected_aya  = 1;
}

var recording = false;

if(age === 0 ||
   gender === null || gender === "" ||
   tongue === null || tongue === "")
{
    window.location.href = "register.html";
}

function recording_update_ui()
{
    if(recording)
    {
        record_btn.style.display = "none";
        stop_btn.style.display = "inline-block";
        next_btn.style.display = "inline-block";
        timer.style.color = "red";
    }
    else
    {
        record_btn.style.display = "inline-block";
        stop_btn.style.display = "none";
        next_btn.style.display = "none";
        timer.style.color = "black";
    }
}

function pad(val)
{
    var valString = val + "";

    if (valString.length < 2)
    {
        return "0" + valString;
    }
    else
    {
        return valString;
    }
}

function update_timer(timer_ctx)
{
    timer_ctx.total_seconds++;
    seconds.innerHTML = pad(timer_ctx.total_seconds % 60);
    minutes.innerHTML = pad(parseInt(timer_ctx.total_seconds / 60));
}

function start_timer(timer_ctx)
{
    timer_ctx.total_seconds = 0;
    timer_ctx.timer_interval = setInterval(update_timer, 1000, timer_ctx);
}

function stop_timer(timer_ctx)
{
    clearInterval(timer_ctx.timer_interval);
    minutes.innerHTML = "00";
    seconds.innerHTML = "00";
}

function _on_next_btn_press(recording_ctx)
{
    setTimeout(() => {
        on_record_complete(recording_ctx);
    }, 500);

    just_a_sec.hidden = false;
    aya_text.hidden = true;

    stop_timer(recording_ctx.timer_ctx);

    if(selected_aya + 1 > suras_ayat[selected_sura])
    {
        if(selected_sura + 1 > 114)
        {
            selected_sura = 1;
            select_option(sura, selected_sura);
            _on_sura_change(1);
            select_option(aya, selected_aya);
            // has to be called after selected_sura/aya
            // have been changed
            prepare_context_and_record();
            return;
        }

        selected_sura++;
        select_option(sura, selected_sura);
        _on_sura_change(selected_sura);
        select_option(aya, selected_aya);
        // has to be called after selected_sura/aya
        // have been changed
        prepare_context_and_record();
        return;
    }

    selected_aya++;
    select_option(aya, selected_aya);
    _on_aya_change(selected_aya);
    // has to be called after selected_sura/aya
    // have been changed
    prepare_context_and_record();
    return;
}

function prepare_context_and_record()
{
    var recording_ctx = {
        recorder: null,

        sura: selected_sura,
        aya: selected_aya,

        timer_ctx: {
            timer_interval: null,
            total_seconds: 0
        }
    };

    stop_btn.onclick = () => {
        _on_stop_btn_press(recording_ctx);
    };

    next_btn.onclick = () => {
        _on_next_btn_press(recording_ctx);
    };

    recording_ctx.recorder = start_recording(() => {
        recording = true;
        just_a_sec.hidden = false;
        aya_text.hidden = true;
        setTimeout(() => {
            just_a_sec.hidden = true;
            aya_text.hidden = false;
            recording_update_ui(true);
            start_timer(recording_ctx.timer_ctx);
        }, 500);
    });
}

function _on_record_btn_press()
{
    clear_message();
    clear_recordings_count();
    clear_uploaded_recordings();

    prepare_context_and_record();
}

function update_message()
{
    const message = document.getElementById('message');
    const recordings_count = Number(local_storage.getItem('recordings_count'));
    const uploaded_recordings = Number(local_storage.getItem('uploaded_recordings'));

    if(recordings_count === uploaded_recordings)
    {
        const total_recordings_count = local_storage.getItem('total_recordings_count');
        const message_text = finished_recording_message(recordings_count, total_recordings_count);
        message.innerHTML = `<p>${message_text}</p>`;
    }
    else
    {
        const message_text = upload_in_progress_message(uploaded_recordings, recordings_count);
        message.innerHTML = `<p>${message_text}</p>`;
    }
}

function clear_message()
{
    const message = document.getElementById('message');
    message.innerHTML = "";
}

function _on_stop_btn_press(recording_ctx)
{
    recording = false;
    recording_update_ui(false);
    stop_timer(recording_ctx.timer_ctx);
    on_record_complete(recording_ctx);
    update_message();
}

function get_aya_text()
{
    aya_text.innerHTML = get_aya_text_(selected_sura, selected_aya);
}

function increment_local_storage_key(key)
{
    let value = local_storage.getItem(key);

    if(value == null || value == '')
    {
        value = 0;
    }

    value = Number(value);
    value++;

    local_storage.setItem(key, value);
}

function increment_recordings_count()
{
    increment_local_storage_key('recordings_count');
    increment_local_storage_key('total_recordings_count');
}

function clear_recordings_count()
{
    local_storage.setItem('recordings_count', 0);
}

function clear_uploaded_recordings()
{
    local_storage.setItem('uploaded_recordings', 0);
}

function _on_upload_complete()
{
    increment_local_storage_key('uploaded_recordings');

    if(!recording)
    {
        update_message();
    }
}

function on_record_complete(recording_ctx)
{
    stop_recording(recording_ctx.recorder, (blob) => {
        upload_recording(blob, recording_ctx.sura, recording_ctx.aya, _on_upload_complete);
    });

    increment_recordings_count();
}

function _on_aya_change(aya_number)
{
    selected_aya = Number(aya_number);
    get_aya_text();
    local_storage.setItem('aya', aya_number);
}

function _on_sura_change(sura_number)
{
    selected_sura = Number(sura_number);
    populate_numbers_dropdown(aya, 1, suras_ayat[selected_sura]);
    local_storage.setItem('sura', sura_number);
    _on_aya_change(1);
}

function select_option(dropdown, value)
{
    var opts = dropdown.options;
    for (var opt, j = 0; opt = opts[j]; j++)
    {
        if (opt.value == value.toString())
        {
            dropdown.selectedIndex = j;
            break;
        }
    }
}

function options_init(lang)
{
    populate_suras(lang);
    populate_numbers_dropdown(aya, 1, suras_ayat[selected_sura]);
    select_option(sura, selected_sura);
    select_option(aya, selected_aya);
}

function _on_language_change(lang)
{
    lang = change_language(lang);
    options_init(lang);
}

window._on_language_change = _on_language_change;
window._on_record_btn_press = _on_record_btn_press;
window._on_sura_change = _on_sura_change;
window._on_aya_change = _on_aya_change;

options_init(window.localStorage.getItem('lang'));
get_aya_text();