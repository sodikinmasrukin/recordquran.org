export {
    change_language,
    populate_numbers_dropdown,
    populate_suras,
    finished_recording_message,
    upload_in_progress_message
};

const local_storage = window.localStorage;

const language_arabic = {
    what_is_this: "ما هذا الموقع؟",
    sura: "السورة:",
    aya: "الآية:",
    record: "سجل",
    stop: "إيقاف",
    next: "التالية",
    dataset: "حمل الأصوات",
    record_my_voice: "سجل صوتي!",
    no_data: "الموقع حديث، لا يوجد تسجيلات بعد :( زرنا لاحقاً.",
    tell_us_about_yourself: "عرفنا بنفسك",
    age: "العمر:",
    gender: "النوع:",
    male: "ذكر",
    female: "انثى",
    tongue: "اللغة الأم:",
    begin: "ابدأ",
    just_a_sec: "لحظة...",
    after_recording_message: "جزاك الله خيراً، لقد سجلت {}. وفي المجمل سجلت {}. عد غداً واقرأ مثلهم.",
    upload_in_progress_message: "جاري رفع التسجيلات ({}/{})، رجاءً لا تغلق الموقع.",
    direction: "rtl",
    sura_list: "suras_ar",
    digits_conversion: arabic_digits,
    ayah_word: ayah_word_arabic
};

const language_english = {
    what_is_this: "What is this?",
    sura: "Sura:",
    aya: "Aya:",
    record: "Record",
    stop: "Stop",
    next: "Next",
    dataset: "Dataset",
    record_my_voice: "Record my voice!",
    no_data: "The website is very new, we still don't have recordings :( Come back soon.",
    tell_us_about_yourself: "Tell us about yourself",
    age: "Age:",
    gender: "Gender:",
    male: "Male",
    female: "Female",
    tongue: "Mother Tongue:",
    begin: "Begin",
    just_a_sec: "Just a second...",
    after_recording_message: "God bless. You've recorded {}. Your total recordings are {}. Please come back tomorrow for more.",
    upload_in_progress_message: "Uploading ayat ({}/{}), please don't close this window yet.",
    direction: "ltr",
    sura_list: "suras_en",
    digits_conversion: null,
    ayah_word: ayah_word_english
};

const available_languages = {
    ar: language_arabic,
    en: language_english
};

function ayah_word_arabic(number_of_ayat)
{
    if(number_of_ayat == 1)
    {
        return "آية واحدة";
    }

    if(number_of_ayat == 2)
    {
        return "آيتين";
    }

    if(number_of_ayat >= 3 && number_of_ayat <= 10)
    {
        return "{} آيات";
    }

    return "{} آية";
}

function ayah_word_english(number_of_ayat)
{
    if(number_of_ayat == 1)
    {
        return "{} Ayah";
    }

    return "{} Ayat";
}

function finished_recording_message(number_of_ayat_today, total_number_of_ayat)
{
    const local_storage = window.localStorage;

    const lang = local_storage.getItem('lang');

    var message = available_languages[lang].after_recording_message;

    const digits_conversion = available_languages[lang].digits_conversion;

    let ayah_word_today;
    let ayah_word_total;
    
    if(digits_conversion === null)
    {
        ayah_word_today = available_languages[lang].ayah_word(number_of_ayat_today);
        ayah_word_today = ayah_word_today.replace('{}', number_of_ayat_today);

        ayah_word_total = available_languages[lang].ayah_word(total_number_of_ayat);
        ayah_word_total = ayah_word_total.replace('{}', total_number_of_ayat);
    }
    else
    {
        ayah_word_today = available_languages[lang].ayah_word(number_of_ayat_today);
        ayah_word_today = ayah_word_today.replace('{}', digits_conversion(number_of_ayat_today));

        ayah_word_total = available_languages[lang].ayah_word(total_number_of_ayat);
        ayah_word_total = ayah_word_total.replace('{}', digits_conversion(total_number_of_ayat));
    }

    message = message.replace('{}', ayah_word_today);
    message = message.replace('{}', ayah_word_total);
    return message;
}

function upload_in_progress_message(uploaded_recordings, recordings_count)
{
    const local_storage = window.localStorage;

    const lang = local_storage.getItem('lang');
    
    var message = available_languages[lang].upload_in_progress_message;

    const digits_conversion = available_languages[lang].digits_conversion;

    if(digits_conversion === null)
    {
        message = message.replace('{}', uploaded_recordings);
        return message.replace('{}', recordings_count);
    }
    else
    {
        message = message.replace('{}', digits_conversion(recordings_count));
        return message.replace('{}', digits_conversion(uploaded_recordings));
    }
}

function place_text(el, config)
{
    const key = el.getAttribute("data-i18n");
    el.innerText = config[key];
}

function place_direction(el, config)
{
    el.style.direction = config.direction;
}

function arabic_digits(en_digits)
{
    const map =
    [
        "&\#1632;","&\#1633;","&\#1634;","&\#1635;","&\#1636;",
        "&\#1637;","&\#1638;","&\#1639;","&\#1640;","&\#1641;"
    ];

    var ar_digits = "";

    en_digits = String(en_digits);

    for(var i = 0; i < en_digits.length; i++)
    {
        ar_digits += map[parseInt(en_digits.charAt(i))];
    }

    return ar_digits;
}

function populate_numbers_dropdown(dropdown, range_start, range_end)
{
    const local_storage = window.localStorage;

    const lang = local_storage.getItem('lang');

    const digits_conversion = available_languages[lang].digits_conversion;

    dropdown.innerHTML = "";
    
    if(digits_conversion === null)
    {
        for(var i = range_start; i <= range_end; i++)
        {
            dropdown.innerHTML += '<option value="' + i + '">' + i + '</option>';
        }
    }
    else
    {
        for(var i = range_start; i <= range_end; i++)
        {
            dropdown.innerHTML += '<option value="' + i + '">' + digits_conversion(i) + '</option>';
        }
    }
}

function populate_suras(lang)
{
    const sura = document.getElementById("sura");

    sura.innerHTML = "";
    
    let options = "";

    for(var i = 1; i <= 114; i++)
    {
        let sura_name = eval("suras_" + lang + "[" + i +"]");
        options += '<option value="' + i + '">' + sura_name + '</option>';
    }

    sura.innerHTML = options;
}

function change_language(lang)
{
    const local_storage = window.localStorage;
    
    if(lang == null || lang == '')
    {
        lang = 'ar';
    }

    local_storage.setItem('lang', lang);

    let language = available_languages[lang];

    const translated_elements = document.querySelectorAll("[data-i18n]");
    translated_elements.forEach(el => place_text(el, language));

    const dir_elements = document.querySelectorAll("[dir]");
    dir_elements.forEach(el => place_direction(el, language));

    return lang;
}

window.change_language = change_language;
change_language(window.localStorage.getItem('lang'));