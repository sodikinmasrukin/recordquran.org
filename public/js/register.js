
import { change_language, populate_numbers_dropdown } from "./languages.js";

const local_storage = window.localStorage;

var age = local_storage.getItem('age');
var gender = local_storage.getItem('gender');
var tongue = local_storage.getItem('tongue');

local_storage.setItem('sura', 1);
local_storage.setItem('aya', 1);

function create_uuid()
{
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}

// all the waves you contribute would be assigned a unique id
// so that a researchers would know that all of these audio files
// come from the same person
local_storage.setItem("id", create_uuid());

function _on_age_change(value)
{
    var age = local_storage.setItem('age', value);
}

function _on_gender_change(value)
{
    var age = local_storage.setItem('gender', value);
}

function _on_tongue_change(value)
{
    var age = local_storage.setItem('tongue', value);
}

function populate_age()
{
    var age_el = document.getElementById("age");
    populate_numbers_dropdown(age_el, 4, 100);
    age_el.innerHTML = '<option value=""></option>' + age_el.innerHTML;
}

function _on_change_language(lang)
{
    change_language(lang);
    populate_age();

    age = local_storage.getItem('age');
    gender = local_storage.getItem('gender');
    tongue = local_storage.getItem('tongue');

    document.getElementById("age").value = age;
    document.getElementById("gender").value = gender;
    document.getElementById("tongue").value = tongue;
}

populate_age();

window._on_tongue_change = _on_tongue_change;
window._on_gender_change = _on_gender_change;
window._on_age_change = _on_age_change;
window._on_change_language = _on_change_language;

document.getElementById("age").value = age;
document.getElementById("gender").value = gender;
document.getElementById("tongue").value = tongue;