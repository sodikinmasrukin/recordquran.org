export { start_recording, stop_recording };

const options = {
    numberOfChannels: 1,
    encoderSampleRate: 48000,
    encoderPath: "js/opus-recorder/encoderWorker.min.js"
};

function start_recording(onstart)
{
    var recorder = new Recorder(options);

    recorder.start().catch((e) => {
        throw 'Error encountered:' + e.message;
    });

    recorder.onstart = onstart;

    return recorder;
}

function stop_recording(recorder, callback)
{
    recorder.stop();

    recorder.ondataavailable = function( typedArray )
    {
        var dataBlob = new Blob( [typedArray], { type: 'audio/ogg' } );
        recorder.close();
        callback(dataBlob);
    };
}