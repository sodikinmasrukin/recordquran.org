<?php

function string_pop(string &$str)
{
    $last_char = substr($str, -1);
    $str = substr($str, 0, -1);
    return $last_char;
}

function user_error_log(string $str)
{
    echo $str;
    error_log($str);
}